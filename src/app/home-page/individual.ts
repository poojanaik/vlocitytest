// Model for Individual person of peoples list
export class Individual{
    public name:string;
    public rating:number;
    public img:string;
    public Description:string;
    public  Likes:string[];
    public  Dislikes:string[]
}