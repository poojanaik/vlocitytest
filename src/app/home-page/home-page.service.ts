// Service for the Home page
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import { People } from './people';
import { Individual } from './individual';

@Injectable()
export class HomePageService {

  constructor(private http: Http) { }

  // Get method
  getPeoplesList(): Observable<People> {
    return this.http.get('/fake-backend/people')
      .map(response => response.json() as People)
      .catch(HomePageService.handleError);
  }

  // Code to handle error
  private static handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      if (error.status === 404) {
        errMsg = `Resource ${error.url} was not found`;
      } else {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
      }
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }

}
