// Code corresponding to home page component
import { Component, OnInit, ViewChild } from '@angular/core';
import { TabsetComponent } from 'ngx-bootstrap';

import { HomePageService } from './home-page.service';

import { Individual } from './individual';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  // Max rating values
  max: number = 5;
  // To make ratings read only
  isReadonly: boolean = true;
  // Value selected in typeahead Search
  selectedValue: string;
  // List of people
  public people: Individual[];
  // Selected Individual to display details
  public selectedIndividual: Individual;

  @ViewChild('staticTabs') staticTabs: TabsetComponent;

  constructor(private homePageService: HomePageService) { }

  ngOnInit() {
    // get Peoples List from fake backend
    this.homePageService.getPeoplesList().subscribe(obj => {
      this.people = obj.People;
      this.selectedIndividual = obj.People[0];
    }, error => console.log('Error Occured'));

  }

  // Individual selected based on click in left Nav
  onSelect(data: Individual) {
    this.selectedIndividual = data;
  }

  // Individual selected based on search result
  onSelectSearch(data: any) {

    this.onSelect(data.item);
    this.selectedValue = ""

    // To highlight the correct left nav tab
    for (let tab of this.staticTabs.tabs) {
      if (tab.heading == data.item.name) {
        tab.active = true;
      } else {
        tab.active = false;
      }
    }
  }

}
