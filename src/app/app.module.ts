import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule }     from './app-routing.module';

import { TypeaheadModule } from 'ngx-bootstrap';
import { RatingModule } from 'ngx-bootstrap/rating';
import { TabsModule } from 'ngx-bootstrap/tabs';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';

import { HomePageService } from './home-page/home-page.service';

// For Mock server
import { MockBackend } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';
import { fakeBackendProvider } from './fake-backend/fake-backend';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    TypeaheadModule.forRoot(),
    RatingModule.forRoot(),
    TabsModule.forRoot()
  ],
  providers: [HomePageService, fakeBackendProvider, MockBackend, BaseRequestOptions ],
  bootstrap: [AppComponent]
})
export class AppModule { }
